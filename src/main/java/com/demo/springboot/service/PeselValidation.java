package com.demo.springboot.service;

public interface PeselValidation {
    boolean checkPesel(String id);
}
