package com.demo.springboot.service.impl;

import com.demo.springboot.service.PeselValidation;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class PeselValidationImpl implements PeselValidation {

    @Override
    public boolean checkPesel(String id) {

        Pattern pattern = Pattern.compile("^[0-9]{11}$");
        Matcher matcher = pattern.matcher(id);
        if (!matcher.find()){
            return false;
        }

        int[] weights = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1};
        String[] digitsStr = id.split("");
        int[] digits = new int[digitsStr.length];

        int i = 0;
        for (String digit : digitsStr){
            digits[i] = Integer.parseInt(digit);
            i++;
        }

        int sum = 0;

        for (int j = 0; j < digits.length; j++){
            sum += digits[j] * weights[j];
        }

        if(sum % 10 == 0){
            return true;
        }
        else {
            return false;
        }

    }
}
